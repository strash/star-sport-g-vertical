extends Control


signal btnback_pressed


enum WHEEL { INCREASE, DECREASE, IDLE }


var POINTER_POS: float # pointer position
var BTN_SPIN_POS: float
var BTN_CLOSE_POS: float

var number_of_sections: int = 12
var pointer_offset: int = 9

var wheel_angle: float = 0.0
var wheel_speed: float = 0.0
var wheel_max_speed: float = 1600.0
var wheel_speed_step: float = 20.0

var wheel_state: int = WHEEL.IDLE


# BUILTINS - - - - - - - - -


func _ready() -> void:
	POINTER_POS = ($Pointer as TextureRect).get_rect().position.y
	BTN_SPIN_POS = ($BtnSpin as Button).get_rect().position.y
	BTN_CLOSE_POS = ($BtnBack as Button).get_rect().position.y
	($Info as Control).call("show_info", false)


func _process(delta: float) -> void:
	# wheel
	if wheel_state != WHEEL.IDLE:
		($Wheel as TextureRect).set_rotation(deg2rad(wheel_angle))
		wheel_angle += delta * wheel_speed
	if wheel_state == WHEEL.INCREASE:
		if wheel_speed > wheel_max_speed:
			wheel_speed = wheel_max_speed
		else:
			wheel_speed += wheel_speed_step
	elif wheel_state == WHEEL.DECREASE:
		if wheel_speed < 0:
			wheel_speed = 0
			wheel_state = WHEEL.IDLE
			var wrapped_angle: float = wrapf(($Wheel as TextureRect).get_rotation(), 0.0, PI * 2)
			($Wheel as TextureRect).set_rotation(wrapped_angle)
			set_nearest_angle(wrapped_angle)
		else:
			wheel_speed -= wheel_speed_step


# METHODS - - - - - - - - -


func set_nearest_angle(angle: float) -> void:
	var angle_in_deg: float = rad2deg(angle)
	var section_angle: float = 360.0 / number_of_sections as float
	var offset: float = fmod(angle_in_deg, section_angle)
	var nearest_angle: float
	if offset > section_angle / 2.0:
		nearest_angle = round( wrapf(angle_in_deg - offset + section_angle, 0.0, 360.0) )
	else:
		nearest_angle = round( wrapf(angle_in_deg - offset, 0.0, 360.0) )
	var _t: int = ($Tween as Tween).interpolate_property($Wheel as TextureRect, "rect_rotation", angle_in_deg, nearest_angle, 0.2)
	wheel_angle = nearest_angle
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	yield(get_tree().create_timer(0.2), "timeout")
	var offseted_angle: float = wrapf(360.0 - (wheel_angle + pointer_offset * section_angle) + 180.0, 0.0, 360.0)
	prints(offseted_angle, offseted_angle / section_angle)
	($Info as Control).call("set_info", offseted_angle / section_angle)
	($Info as Control).call("show_info", true)


func load_level() -> void:
	var _t: int = ($Tween as Tween).interpolate_property($Wheel as TextureRect, "rect_scale", Vector2.ZERO, Vector2.ONE, 0.3)
	_t = ($Tween as Tween).interpolate_property($Wheel as TextureRect, "modulate:a", 0.0, 1.0, 0.3)
	_t = ($Tween as Tween).interpolate_property($Pointer as TextureRect, "rect_position:y", null, POINTER_POS, 0.3)
	_t = ($Tween as Tween).interpolate_property($BtnSpin as Button, "rect_position:y", null, BTN_SPIN_POS, 0.3)
	_t = ($Tween as Tween).interpolate_property($BtnBack as Button, "rect_position:y", null, BTN_CLOSE_POS, 0.3)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


func clear_level() -> void:
	var _t: int = ($Tween as Tween).interpolate_property($Wheel as TextureRect, "rect_scale", Vector2.ONE, Vector2.ZERO, 0.3)
	_t = ($Tween as Tween).interpolate_property($Wheel as TextureRect, "modulate:a", 1.0, 0.0, 0.3)
	_t = ($Tween as Tween).interpolate_property($Pointer as TextureRect, "rect_position:y", null, -150.0, 0.3)
	_t = ($Tween as Tween).interpolate_property($BtnSpin as Button, "rect_position:y", null, get_viewport_rect().size.y, 0.3)
	_t = ($Tween as Tween).interpolate_property($BtnBack as Button, "rect_position:y", null, get_viewport_rect().size.y, 0.3)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# SIGNALS - - - - - - - - -


func _on_BtnSpin_pressed() -> void:
	if wheel_state == WHEEL.IDLE:
		wheel_state = WHEEL.INCREASE
		yield(get_tree().create_timer(rand_range(2.8, 4.0)), "timeout")
		wheel_state = WHEEL.DECREASE


func _on_BtnBack_pressed() -> void:
	if wheel_state == WHEEL.IDLE:
		emit_signal("btnback_pressed")
