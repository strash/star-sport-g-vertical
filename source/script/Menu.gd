extends Control


signal btnplay_pressed


var logo_pos: float = 110.0
var logo_offset: float = 20.0
var angle: float = 0.0


# BUILTINS - - - - - - - - -


func _ready() -> void:
	logo_pos = ($Logotype as TextureRect).get_rect().position.y


func _process(delta: float) -> void:
	$Logotype.rect_position.y = logo_pos + sin(angle) * logo_offset
	angle += delta * 1.5


# METHODS - - - - - - - - -


# SIGNALS - - - - - - - - -


func _on_BtnPlay_pressed() -> void:
	emit_signal("btnplay_pressed")
