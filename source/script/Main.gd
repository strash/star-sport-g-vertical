extends Node


enum VIEWS { MENU, GAME }


# BUILTINS - - - - - - - - -


func _ready() -> void:
	var _btnplay_pressed: int = $Menu.connect("btnplay_pressed", self, "_on_Menu_btnplay_pressed")
	var _game_btnback_pressed: int = $Game.connect("btnback_pressed", self, "_on_Game_btnback_pressed")
	show_view(VIEWS.MENU)
	$Game.clear_level()


# METHODS - - - - - - - - -


func show_view(view: int) -> void:
	match view:
		VIEWS.MENU:
			$Menu.show()
			$Tween.interpolate_property($Menu, "modulate:a", 0.0, 1.0, 0.5)
			$Tween.interpolate_property($Game, "modulate:a", 1.0, 0.0, 0.5)
			if not $Tween.is_active():
				$Tween.start()
			yield(get_tree().create_timer(0.5), "timeout")
			$Game.hide()
		VIEWS.GAME:
			$Game.show()
			$Tween.interpolate_property($Menu, "modulate:a", 1.0, 0.0, 0.5)
			$Tween.interpolate_property($Game, "modulate:a", 0.0, 1.0, 0.5)
			if not $Tween.is_active():
				$Tween.start()
			yield(get_tree().create_timer(0.5), "timeout")
			$Menu.hide()


# SIGNALS - - - - - - - - -


func _on_Menu_btnplay_pressed() -> void:
	show_view(VIEWS.GAME)
	$Game.load_level()


func _on_Game_btnback_pressed() -> void:
	$Game.clear_level()
	yield(get_tree().create_timer(0.25), "timeout")
	show_view(VIEWS.MENU)
