extends Control


const MODALS: Array = [
	preload("res://source/assets/image/modal/1.png"),
	preload("res://source/assets/image/modal/2.png"),
	preload("res://source/assets/image/modal/3.png"),
	preload("res://source/assets/image/modal/4.png"),
	preload("res://source/assets/image/modal/5.png"),
	preload("res://source/assets/image/modal/6.png"),
	preload("res://source/assets/image/modal/7.png"),
	preload("res://source/assets/image/modal/8.png"),
	preload("res://source/assets/image/modal/9.png"),
	preload("res://source/assets/image/modal/10.png"),
	preload("res://source/assets/image/modal/11.png"),
	preload("res://source/assets/image/modal/12.png"),
]

var view: Vector2


# BUILTINS - - - - - - - - -


func _ready() -> void:
	view = get_viewport_rect().size
	set_info(0)


# METHODS - - - - - - - - -


func show_info(state: bool) -> void:
	var _t: int
	if state:
		$".".show()
		_t = ($Tween as Tween).interpolate_property(self, "modulate:a", null, 1.0, 0.3)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
	else:
		_t = ($Tween as Tween).interpolate_property(self, "modulate:a", null, 0.0, 0.3)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()
		yield(get_tree().create_timer(0.3), "timeout")
		$".".hide()


func set_info(indise: int) -> void:
	($BtnClose/Modal as TextureRect).texture = MODALS[indise]
	($BtnClose/Modal as TextureRect).rect_size = Vector2.ZERO
	($BtnClose as Button).rect_size = ($BtnClose/Modal as TextureRect).rect_size
	($BtnClose as Button).rect_position = (view - ($BtnClose as Button).rect_size) / 2.0
	($BtnClose/Modal as TextureRect).rect_position = Vector2.ZERO


# SIGNALS - - - - - - - - -


func _on_BtnClose_pressed() -> void:
	show_info(false)
